package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

type error struct {
	Message string `json:"message"`
	Method  string `json:"method"`
	Line    string `json:"line"`
}

func newErrorResponse(c *gin.Context, statusCode int, msg, method, line string) {
	logrus.Errorf(msg)
	c.AbortWithStatusJSON(statusCode, error{msg, method, line})
}
