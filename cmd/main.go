package main

import (
	"github.com/joho/godotenv"
	_ "github.com/lib/pq"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"gitlab.com/farkhutdinov/todo-app"
	"gitlab.com/farkhutdinov/todo-app/pkg/handler"
	"gitlab.com/farkhutdinov/todo-app/pkg/repository"
	"gitlab.com/farkhutdinov/todo-app/pkg/service"
	"os"
)

const DB_ENV = "db.env"

func main() {
	initLogrus()

	if err := initConfig(); err != nil {
		logrus.Fatalf("error initializing config %s", err.Error())
	}

	if err := godotenv.Load(DB_ENV); err != nil {
		logrus.Fatalf("error initializing %s %s", DB_ENV, err.Error())
	}
	db, err := repository.NewPostgresDB(repository.Config{
		Host:     viper.GetString("db.host"),
		Port:     viper.GetString("db.port"),
		Username: viper.GetString("db.username"),
		Password: os.Getenv("DB_PASSWORD"),
		DBName:   viper.GetString("db.dbname"),
		SSLMode:  viper.GetString("db.sslmode"),
	})
	if err != nil {
		logrus.Fatalf("error initializing DB %s", err.Error())
	}

	repos := repository.NewRepository(db)
	services := service.NewService(repos)
	handlers := handler.NewHandler(services)

	srv := new(todo_app.Server)

	if err := srv.Run(viper.GetString("port"), handlers.InitRoutes()); err != nil {
		logrus.Fatalf("error occured while runnig htpp server: %s", err)
	}
}

//Инициализация логирования
func initLogrus() {
	logrus.SetFormatter(new(logrus.JSONFormatter))
}

//Инициализация файла .cfg
func initConfig() error {
	viper.AddConfigPath("configs")
	viper.SetConfigName("config")
	return viper.ReadInConfig()
}
