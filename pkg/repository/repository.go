package repository

import (
	"github.com/jmoiron/sqlx"
	todo "gitlab.com/farkhutdinov/todo-app"
)

type Authorization interface {
	CreateUser(user todo.User) (int, error)
	GetUser(username, password string) (todo.User, error)
}

type TodoList interface {
}

type TodoItem interface {
}

type Auth struct {
	Authorization
	TodoItem
	TodoList
}

func NewRepository(db *sqlx.DB) *Auth {
	return &Auth{Authorization: NewAuthPostgres(db)}
}
