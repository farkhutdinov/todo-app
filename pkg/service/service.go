package service

import (
	todo "gitlab.com/farkhutdinov/todo-app"
	"gitlab.com/farkhutdinov/todo-app/pkg/repository"
)

type Authorization interface {
	CreateUser(input todo.User) (int, error)
	GenerateToken(username string, password string) (string, error)
	ParseToken(token string) (int, error)
}
type TodoList interface {
}
type TodoItem interface {
}

type Service struct {
	Authorization
	TodoItem
	TodoList
}

func NewService(repos *repository.Auth) *Service {
	return &Service{Authorization: NewAuthService(repos.Authorization)}
}
